import sys
import pprint
import copy
import re
sys.displayhook = pprint.pprint

#Warning: Runs exclusively in Python 2.7.x

project_dir = '/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/'
code_dir = project_dir + 'Code/'
input_dir = project_dir + 'Data/Inputs/'
output_dir = project_dir + 'Data/Outputs/'
tools_dir = project_dir + 'Tools/'
matconvnet_path = tools_dir + 'Matconvnet/'
mappings_dir = input_dir + 'SBIR/'
word2vec_dir = input_dir + 'Word2vec/'

var = locals()
f=open(input_dir+'dirlist.txt','w+')

for key,value in var.items():
	if key.startswith('__') == 0 and key.startswith('var') == 0 and str(value) == value: 
			f.write((str(key)+'='+str(value))+'\n')

f.close()

