import multiprocessing as mp


def parfor(func,data)
        num_cores = mp.cpu_count()
        pool = mp.Pool(processes=num_cores)
        pool.map(func,data)
        finally:
            pool.close()
            pool.join()