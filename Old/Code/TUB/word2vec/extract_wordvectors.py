import gensim
import bashutils
import logging
import numpy as np
import IO
#import parallellib


class Wordvectors():
    """
    Purpose:

    Variables:

    I/O:
            Number of Parameters Taken: 3.
            input_file (Required) -> It is the txt file which contains the names of the words whose wordvectors need to be extracted.
            output_file (Required) -> It is the name of the .mat file which will be given as the output
            wordvec_size (Default: 300) -> It is the length of each wordvector formed.
    """

    def __init__(self, input_file, output_file, model_path, wordvec_size):
        self.input = input_file
        self.output = output_file
        self.model_path = model_path
        self.wordvec_size = wordvec_size
        self.dictionary = {}

    def parse_input(self):
        self.fd = open(self.input)
        self.file_size = bashutils.line_count(self.input)
        self.wordvectors = np.zeros((self.wordvec_size, self.file_size), dtype=np.float32)
        self.lines = self.fd.read().split('\n')[:-1]
        assert(len(self.lines) == self.file_size)

    def gen_all(self):
        for line in self.lines:
            self.get_wordvec(line)

    def get_wordvec(self,line):
        line = line.split(':')
        word = line[0]
        num = int(line[1])
        #print(word)
        wordvec = self.model[word]
        wordvec = np.array(wordvec)
        self.wordvectors[:,int(num)-1] = wordvec[:]
        self.dictionary[str(num)] = word

    def loadmodel(self):
        self.model = gensim.models.Word2Vec.load_word2vec_format(self.model_path, binary=True)

    def save(self):
        IO.save(self.output, 'features', self.wordvectors)

f = open('dirlist.txt')
lines = f.read().split('\n')[:-1]

dic = {}

for line in lines:
	line=line.split('=')
	dic[line[0]]=line[1]

v = Wordvectors(dic['word2vec_dir'] + 'wordvectors.txt', dic['output_dir']+'Word2vec/word_vectors', dic['word2vec_dir']+'google.bin', 300)
v.parse_input()
v.loadmodel()
v.gen_all()
v.save()

