function [features] = getFeaturesComplex(net,imdb,layer)

addpath(genpath('~/TMH/matconvnet-master/'));
run('vl_setupnn.m');  

net.layers{end}.type = 'softmax';
numImages = size(imdb.images.data,4);
disp(numImages);

im2 = imdb.images.data(:,:,:,1);
im = zeros(256,256,6,'uint8');
labels = imdb.images.labels(:,:,:,1);
im = uint8(255*im2(:,:,:));
im = (single(im));
im = 255 - im;
WH = 225;
RS = 256-WH+1;
im_new = gpuArray(zeros(WH,WH,size(im,3),'single')); 
    x = 16;
    y = 16;
    roll = rand();
    if roll>0.45
        tmpImg = imrotate(im(:,:,:),randi([-15 15],1,1),'bilinear','crop');
    else
        tmpImg = im(:,:,:);
    end
    im_new(:,:,:) = tmpImg(x:(x+WH-1),y:(y+WH-1),:);
    roll = rand();
    if roll>0.5
        for j=1:6
            im_new(:,:,j) = fliplr(im_new(:,:,j));
        end
    end
res=vl_simplenn(net,im_new);
featureSize = numel(res(layer).x);
features = zeros(numImages,featureSize);

for i = 1:numImages
    im2 = imdb.images.data(:,:,:,i);
    im = zeros(256,256,6,'uint8');
    im = uint8(255*im2(:,:,:));
    im = (single(im));
    im = 255 - im;
    WH = 225;
    RS = 256-WH+1;
    im_new = gpuArray(zeros(WH,WH,size(im,3),'single')); 
    x = 16;
    y = 16;
    roll = rand();
    if roll>0.45
        tmpImg = imrotate(im(:,:,:),randi([-15 15],1,1),'bilinear','crop');
    else
        tmpImg = im(:,:,:);
    end
    im_new(:,:,:) = tmpImg(x:(x+WH-1),y:(y+WH-1),:);
    roll = rand();
    if roll>0.5
        for j=1:6
            im_new(:,:,j) = fliplr(im_new(:,:,j));
        end
    end
    res=vl_simplenn(net,im_new);
    feature = squeeze(gather(res(layer).x));
    new_feature = zeros(1,featureSize);
    new_feature = reshape(feature,1,featureSize);
    features(i,:) = feature;
    disp(i);
end

end
