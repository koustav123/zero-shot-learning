function [net, info] = cnn_sketch_zs(varargin)
addpath(genpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/'));
run('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab/vl_setupnn.m') ;

opts.expDir = fullfile('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/SBIR/finetunewordvec_TUB_TMH_0.001_128_ZS_round4');
opts.imdbPath = fullfile('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/wordvec_TUB/TUB_DATA_MORPHED_ZS.mat');
opts.netPath = ('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/SBIR/TUB_TMH_0.001_128_ZS_round4/net-epoch-81.mat');
opts.train.batchSize = 128 ;
opts.train.numEpochs = 1000;
opts.train.continue = true ;
opts.train.gpus = [1] ;
opts.cudnn = true ;
opts.train.learningRate = [0.00001*ones(1,2) 0.02]; 
opts.train.expDir = opts.expDir ;
opts = vl_argparse(opts, varargin) ;


load(opts.imdbPath) ;
make_zero_shot;
idx = zeros(1,20000);
for i=1:20000
    idx(1,i)=i;
end

imdb.images.idx = idx;

load(opts.netPath) ;
scal = 1 ;
bias = 0.01;

net.layers{21} = [];
net.layers{20} = [];

net.layers{20} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(1,1,512,300,'single'), ...
                           'biases', bias*ones(1, 300, 'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 10, ...
                           'biasesLearningRate', 20, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;

%net.layers{end+1} = struct('type', 'softmaxloss') ;
ly.type = 'custom' ;
    ly.forward = @nndistance_forward ;
    ly.backward = @nndistance_backward ;
     
net.layers{21} = ly ;

net = vl_simplenn_tidy(net);

[net, info] = cnn_train(net, imdb, @getBatch, ...
    opts.train, ...
    'val', find(imdb.images.set == 3)) ;


function [im, labels] = getBatch(imdb, batch)

N = length(batch);
im2 = imdb.images.data(:,:,:,batch) ;
im = zeros(256,256,6,N,'uint8');
labels = imdb.images.wordvec_labels(:,:,:,batch);
im = uint8(255*im2(:,:,:,:));
im = 255 - im;
im = gpuArray(single(im));
WH = 225;
RS = 256-WH+1;
im_new = gpuArray(zeros(WH,WH,size(im,3),N,'single')); 
for i = 1:N
    x = randperm(RS,1);
    y = randperm(RS,1);
    roll = rand();
    if roll>0.45
        tmpImg = imrotate(im(:,:,:,i),randi([-10 10],1,1),'bilinear','crop');
    else
        tmpImg = im(:,:,:,i);
    end
    
    im_new(:,:,:,i) = tmpImg(x:(x+WH-1),y:(y+WH-1),:);
    roll = rand();
    if roll>0.5
        for j=1:6
            im_new(:,:,j,i) = fliplr(im_new(:,:,j,i));
        end
    end
end
im = im_new;