function [features] = getFeatures(layers)

addpath(genpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/'));
run('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab/vl_setupnn.m') ;
load('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/wordvec_TUB/TUB_morphed_wordvec_imdb.mat');
load('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/SBIR/TUB_wordvec_0.01_128_finetune_70/net-epoch-364.mat');
tic;
net = vl_simplenn_move(net,'gpu') ;
numImages = size(imdb.images.data,4);

im = imdb.images.data(:,:,:,:) ;
%testY = imdb.images.labels(:,:);
testY = imdb.images.wordvec_labels(:,:,:,:);
sizeLabels=size(testY);
clear imdb;

testimg = im(:,:,:,1);
testimg = uint8(255*testimg);
testimg = 255-testimg;
im1 = testimg(1:225,1:225,:) ;
im2 = testimg(32:256,1:225,:) ;
im3 = testimg(1:225,32:256,:) ;
im4 = testimg(32:256,32:256,:) ;
im5 = testimg(16:240,16:240,:) ;
im15 = cat(4,im1,im2,im3,im4,im5);
im_new = cat(4,im15,fliplr(im15));
im_new = gpuArray(single(im_new));
    
net.layers{end}.class = zeros(sizeLabels(1),sizeLabels(2),sizeLabels(3),10);
net.layers{end}.w = zeros(sizeLabels(1),sizeLabels(2),sizeLabels(3),10);

res = vl_simplenn(net, im_new, [], [], ...
                  'mode', 'test', ...
                  'conserveMemory', false, ...
                  'sync', true, ...
                  'cudnn', true) ;
              
featureSize = size(res(layers(1)).x,3);
noImgs = size(res(layers(1)).x,4);
numLayers = size(layers,2);
features = zeros(numLayers,numImages,featureSize*noImgs);
              
predY = zeros(1,length(testY));

for i = 1:length(testY)
    if mod(i,1000) == 0
        disp(i);
    end
    test = im(:,:,:,i);
    test = uint8(255*test);
    testX = 255-test;
    im1 = testX(1:225,1:225,:) ;
    im2 = testX(32:256,1:225,:) ;
    im3 = testX(1:225,32:256,:) ;
    im4 = testX(32:256,32:256,:) ;
    im5 = testX(16:240,16:240,:) ;
    im15 = cat(4,im1,im2,im3,im4,im5);
    im_new = cat(4,im15,fliplr(im15));
    im_new = gpuArray(single(im_new));
    
    %net.layers{end}.class = kron(ones(1,size(im_new,4)),testY(:,:,:,i));
    net.layers{end}.class = zeros(sizeLabels(1),sizeLabels(2),sizeLabels(3),10);
    net.layers{end}.w = zeros(sizeLabels(1),sizeLabels(2),sizeLabels(3),10);

    res = vl_simplenn(net, im_new, [], [], ...
                      'mode', 'test', ...
                      'conserveMemory', false, ...
                      'sync', true, ...
                      'cudnn', true) ;
    for j = 1:numLayers
        feature = squeeze(gather(res(layers(j)).x));
        feature = reshape(feature,1,[]);
        features(j,i,:) = squeeze(feature);
    end
end

toc;

