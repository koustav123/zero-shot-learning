import h5py, numpy as np
from sklearn.cross_validation import train_test_split
from sklearn import preprocessing
from sklearn import svm
import scipy.io
import os
import sys
import dirlist
from sklearn import cross_validation
from sklearn import grid_search
import warnings
warnings.filterwarnings("ignore")

save_data = 1;

class Classify():
    """
    TODO: Change this class document and add the __init__ function to initialize all the input variables.
    Purpose:

    Variables:

    I/O:
            Number of Parameters Taken: 3.
            input_file (Required) -> It is the txt file which contains the names of the words whose wordvectors need to be extracted.
            output_file (Required) -> It is the name of the .mat file which will be given as the output
            wordvec_size (Default: 300) -> It is the length of each wordvector formed.
    """

    def load_data_wordvec(self,feature_file,imdb_file): #Only this function and prepare_features is specific to Word2Vec, otherwise this code is so general, it must be capable of handling anything
        """
        Fast I/O required for multiple experiments to be performed.
        """
        print('Loading data..')
        fp = h5py.File(imdb_file+'.mat','r')
        f = h5py.File(feature_file+'.mat','r')
        self.labels = np.array(fp.get('imdb/images/labels'))
        self.splits = np.array(fp.get('imdb/images/set'))
        self.features = np.array(f.get('Features'))
        self.features = self.features.swapaxes(0,1)
        print('Data Loaded!!')

    def select_features(self,mode,train,no): #Add defaults
        if train == 1:
            if mode == 'simple':
                return self.features[self.train_idx,:,no]

        else:
            if mode == 'simple':
                return self.features_simple[self.test_idx,:,no]
    
    def prepare_features(self):
        self.train_idx = np.array(np.nonzero(self.splits == 1))[0]
        self.test_idx = np.array(np.nonzero(self.splits == 3))[0]
        for j in xrange(6):
            self.X_train = self.select_features('simple',1,j)
            self.X_test = self.select_features('simple',0,j)
            self.Y_train = self.labels[self.train_idx]
            self.Y_test = self.labels[self.test_idx]

        print('Data ready!')

    def preprocess_data(self):
        """
        Try different preprocessing schemes, whichever works the best should be adopted.
        TODO: Add variables for File Names and mdict names too.
        """
        print('Preprocessing Data...')
        
        self.labels = self.labels.flatten() #Convert to a 1-D array.
        self.X_train = preprocess_mode('scale',self.X_train)
        self.X_test = preprocess_mode('scale',self.X_test)

        if save_data == 1: 
            scipy.io.savemat('features_train.mat',mdict={'WordVecFeatures' : [X_train,Y_train]})
            scipy.io.savemat('features_test.mat',mdict={'WordVecFeatures' : [X_test,Y_test]})
        
        print('Preprocessing finished!')
    
    def debug(self,debug_mode):
        """
        Do all the assertions and testing of code here. Every function should have a test case to ensure it is working good.
        TODO: Make this!
        """
        if debug_mode == 1: 
            if X_train.shape != []:
                print('Error in the shape of X_train')
                #TODO: Do it for all variables, basically do all the testing part, with all the asserts and all here.

    def preprocess_mode(self,mode,features): #Add defaults and other modes as we go along
        if mode == 'scale':
            return preprocessing.scale(features)

    

    def classifier_train(self,classifier,mode,parameters):
        #TODO: How to insert parameters specific to classifiers? Maybe fishy, but okay.
        # Add defaults.
        if classifier == 'SVM':
            if mode == 'Gridsearch':
                print('Training SVM... Mode: Grid Search')
                classifier = svm.SVC()
                classifier = grid_search.GridSearchCV(clf, parameters)
                #parameters = {'kernel':['linear','rbf','poly','sigmoid'], 'degree':[1,2,3,4]} #Some standard parameters for refrence. Code this up. TODO
                print clf.best_params_
                print(clf.score(X_test, Y_test))

            if mode == 'Cross Validate':
                print('Training SVM... Mode: Cross Validate')
                scores = cross_validation.cross_val_score(self.classifier, np.concatenate((X_train,X_test),axis=1), np.concatenate((Y_train,Y_test),axis=1), cv= 5, scoring="accuracy")
                print scores
            
            if mode == 'Train':
                print('Training SVM... Mode: Train')

        if classifier == 'RandomForest':
            classifier=RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1)
            classifier.fit(X_train , Y_train)

        print('Finished Training!')

A = Classify()
A.load_data_wordvec('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Code/feeatures/TUB_70finetune_218epoch_WordVec_512_4layers_features.mat','/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/wordvec_TUB/TUB_morphed_wordvec_imdb.mat')
A.prepare_features()
A.preprocess_data()

