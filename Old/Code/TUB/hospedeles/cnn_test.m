clc;clear;
addpath(genpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/'));
run('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab/vl_setupnn.m') ;
load('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/wordvec_TUB/TUB_morphed_wordvec_imdb.mat')
load('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/SBIR/TUB_wordvec_0.01_128_reverse_finetune_1075/net-epoch-160.mat');

net = vl_simplenn_move(net, 'gpu') ;

N=numel(find(imdb.images.set == 3));
im = imdb.images.data(:,:,:,imdb.images.set==3) ;
testY = imdb.images.labels(:,imdb.images.set==3);
clear imdb;
im = uint8(255*im(:,:,:,:));
testX = 255-im;
clear im;

predY = zeros(1,length(testY));

for i = 1:length(testY)
    if mod(i,1000) == 0
        disp(i);
    end
    testimg = testX(:,:,:,i);
    im1 = testimg(1:225,1:225,:) ;
    im2 = testimg(32:256,1:225,:) ;
    im3 = testimg(1:225,32:256,:) ;
    im4 = testimg(32:256,32:256,:) ;
    im5 = testimg(16:240,16:240,:) ;
    im15 = cat(4,im1,im2,im3,im4,im5);
    im_new = cat(4,im15,fliplr(im15));
    im_new = gpuArray(single(im_new));
    
    net.layers{end}.class = kron(ones(1,size(im_new,4)),testY(i));
    res = vl_simplenn(net, im_new, [], [], ...
                      'mode', 'test', ...
                      'conserveMemory', false, ...
                      'sync', true, ...
                      'cudnn', true) ;
    pred = squeeze(gather(res(end-1).x));
    pred = sum(pred,2);
    [~,pred] = max(pred);
    predY(i) = pred;
end

mean(predY==testY)
