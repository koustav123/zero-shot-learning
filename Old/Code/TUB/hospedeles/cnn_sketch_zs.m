function [net, info] = cnn_sketch_zs(varargin)
addpath(genpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/'));
run('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab/vl_setupnn.m') ;

opts.expDir = fullfile('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/SBIR/TUB_TMH_0.001_128_ZS_round5');
opts.imdbPath = fullfile('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/wordvec_TUB/TUB_DATA_MORPHED_ZS.mat');
opts.train.batchSize = 192 ;
opts.train.numEpochs = 1000;
opts.train.continue = true ;
opts.train.gpus = [1] ;
opts.cudnn = true ;
opts.train.learningRate = 0.001 ; 
opts.train.expDir = opts.expDir ;
opts = vl_argparse(opts, varargin) ;


load(opts.imdbPath) ;
make_zero_shot;
idx = zeros(1,20000);
for i=1:20000
    idx(1,i)=i;
end

imdb.images.idx = idx;

scal = 1 ;
bias = 0.01;
net.layers = {} ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(15, 15, size(imdb.images.data,3), 64, 'single'), ...
                           'biases', bias*ones(1, 64, 'single'), ...
                           'stride', 3, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(5, 5, 64, 128, 'single'), ...
                           'biases', bias*ones(1, 128, 'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;
                       

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(3,3,128,256,'single'), ...
                           'biases', bias*ones(1,256,'single'), ...
                           'stride', 1, ...
                           'pad', 1, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(3,3,256,256,'single'), ...
                           'biases', bias*ones(1,256,'single'), ...
                           'stride', 1, ...
                           'pad', 1, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(3,3,256,256,'single'), ...
                           'biases', bias*ones(1,256,'single'), ...
                           'stride', 1, ...
                           'pad', 1, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(7,7,256,512,'single'),...
                           'biases', bias*ones(1,512,'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.5) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(1,1,512,512,'single'),...
                           'biases', bias*ones(1,512,'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.5) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(1,1,512,250,'single'), ...
                           'biases', bias*ones(1, 250, 'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;

net.layers{end+1} = struct('type', 'softmaxloss') ;

net = vl_simplenn_tidy(net);

[net, info] = cnn_train(net, imdb, @getBatch, ...
    opts.train, ...
    'val', find(imdb.images.set == 3)) ;



function [im, labels] = getBatch(imdb, batch)

N = length(batch);
im2 = imdb.images.data(:,:,:,batch) ;
im = zeros(256,256,6,N,'uint8');
labels = imdb.images.labels(1,batch) ;
im = uint8(255*im2(:,:,:,:));
im = gpuArray(single(im));
im = 255 - im;
WH = 225;
RS = 256-WH+1;
im_new = gpuArray(zeros(WH,WH,size(im,3),N,'single')); 
for i = 1:N
    x = randperm(RS,1);
    y = randperm(RS,1);
    roll = rand();
    if roll>0.45
        tmpImg = imrotate(im(:,:,:,i),randi([-10 10],1,1),'bilinear','crop');
    else
        tmpImg = im(:,:,:,i);
    end
    im_new(:,:,:,i) = tmpImg(x:(x+WH-1),y:(y+WH-1),:);
    roll = rand();
    if roll>0.5
        for j=1:6
            im_new(:,:,j,i) = fliplr(im_new(:,:,j,i));
        end
    end
end
im = im_new;

