clc;clear;
load('features_epoch356_full.mat');
numclasses=250;

for j = 1 : 6 
    for i=1:numclasses
        disp(i);
        idx_train=find(imdb.images.actlabels==i & imdb.images.set==1);
        idx_test=find(imdb.images.actlabels==i & imdb.images.set==3);
        wordvec=reshape(imdb.images.labels(:,:,:,idx_train(1)),[],1);
        mean_train(:,i)=mean(imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(j,idx_train,:));
        mean_test(:,i)=mean(imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(j,idx_test,:));
        train_diff(i)=sum(abs(mean_train(:,i)-wordvec));
        test_diff(i)=sum(abs(mean_test(:,i)-wordvec));
        var_train(i)=sum(mean(abs(bsxfun(@minus,imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(j,idx_train,:),wordvec))));
        var_test(i)=sum(mean(abs(bsxfun(@minus,imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(j,idx_test,:),wordvec))));
    end
    figure;
    subplot(2,2,1);
    plot(train_diff);
    title('Mean in Training');
    subplot(2,2,2);
    plot(test_diff);
    title('Mean in Testing');
    subplot(2,2,3);
    plot(var_train);
    title('Variance in Training');
    subplot(2,2,4);
    plot(var_test);
    title('Variance in Testing');
end