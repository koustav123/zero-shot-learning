function [net, info] = cnn_sketch(varargin)
addpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab');
addpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/examples');
addpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/')
run('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab/vl_setupnn.m') ;

opts.expDir = fullfile('../../../Data/wordvec_Caltech/caltech80_vggs_bn_0.00001_128/') ;
opts.imdbPath = fullfile('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/wordvec_Caltech/caltech80_wordvec.mat');
opts.netPath = ('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/wordvec_Caltech/imagenet-matconvnet-vgg-s.mat');
opts.train.batchSize = 16 ;
opts.train.numEpochs = 1000;
opts.train.continue = true ;
opts.train.gpus = [1] ;
opts.cudnn = true ;
opts.train.learningRate = 0.00001 ; 
opts.train.expDir = opts.expDir ;
opts = vl_argparse(opts, varargin) ;

load(opts.imdbPath) ;
imdb.images.set=imdb.images.sets;
net=load(opts.netPath) ;
scal = 1 ;
bias = 0.01;

net.layers{18} = [];
net.layers{19} = [];

net.layers{1,18} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(1,1,4096,257,'single'), ...
                           'biases', bias*ones(1, 257, 'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 100, ...
                           'biasesLearningRate', 200, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;

net.layers{19} = struct('type', 'softmaxloss') ;
%ly.type = 'custom' ;
%    ly.forward = @nndistance_forward ;
%    ly.backward = @nndistance_backward ;
     
%net.layers{19} = ly ;

net = vl_simplenn_tidy(net);

[net, info] = cnn_train(net, imdb, @getBatch, ...
    opts.train, ...
    'val', find(imdb.images.set == 3)) ;


function [im_new, labels] = getBatch(imdb, batch)

N = length(batch);
im = imdb.images.data(:,:,:,batch);
labels = single(imdb.images.labels(batch));
im = single(im);
WH = 224;
RS = 227-WH+1;
im_new = gpuArray(zeros(WH,WH,size(im,3),N,'single')); 
for i = 1:N
    x = randperm(RS,1);
    y = randperm(RS,1);
    roll = rand();
    if roll>0.45
        tmpImg = imrotate(im(:,:,:,i),randi([-10 10],1,1),'bilinear','crop');
    else
        tmpImg = im(:,:,:,i);
    end
    im_new(:,:,:,i) = gpuArray(tmpImg(x:(x+WH-1),y:(y+WH-1),:));
    roll = rand();
    if roll>0.5
        for j=1:3
            im_new(:,:,j,i) = fliplr(im_new(:,:,j,i));
        end
    end
end


