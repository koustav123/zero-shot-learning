f = open('mapping_CCA.txt')
lines = f.read().split('\n')

TUB_label = []
Caltech_label = []

cal_set1 = []
cal_set2 = []
cal_set3 = []
cal_set4 = []
cal_set5 = []
tot_set = []

for line in lines:
	tub = int(line.split(':')[3])
	cal = int(line.split(':')[2])
	if tub <= 50:
		cal_set1.append(cal)
		tot_set.append(cal)
	elif tub <= 100:
		cal_set2.append(cal)
		tot_set.append(cal)
	elif tub <= 150:
		cal_set3.append(cal)
		tot_set.append(cal)
	elif tub <= 200:
		cal_set4.append(cal)
		tot_set.append(cal)
	elif tub <= 250:
		cal_set5.append(cal)
		tot_set.append(cal)	

for i in xrange(257):
	j = i+1
	setno = 1
	if len(cal_set1)>=50:
		setno+=1
	if len(cal_set2)>=50:
		setno+=1
	if len(cal_set3)>=50:
		setno+=1
	if len(cal_set4)>=50:
		setno+=1
	if len(cal_set5)>=50:
		setno+=1
	if setno == 1 and j not in cal_set1 and j not in cal_set2 and j not in cal_set3 and j not in cal_set4 and j not in cal_set5:
		cal_set1.append(j)
		tot_set.append(j)
	elif setno == 2 and j not in cal_set1 and j not in cal_set2 and j not in cal_set3 and j not in cal_set4 and j not in cal_set5:
		cal_set2.append(j)
		tot_set.append(j)
	elif setno == 3 and j not in cal_set1 and j not in cal_set2 and j not in cal_set3 and j not in cal_set4 and j not in cal_set5:
		cal_set3.append(j)
		tot_set.append(j) 
	elif setno == 4 and j not in cal_set1 and j not in cal_set2 and j not in cal_set3 and j not in cal_set4 and j not in cal_set5:
		cal_set4.append(j)
		tot_set.append(j)
	elif setno == 5 and j not in cal_set1 and j not in cal_set2 and j not in cal_set3 and j not in cal_set4 and j not in cal_set5:
		cal_set5.append(j)
		tot_set.append(j) 

print cal_set1
print cal_set2
print cal_set3
print cal_set4
print cal_set5
print tot_set
print len(cal_set1)
print len(cal_set2)
print len(cal_set3)
print len(cal_set4)
print len(cal_set5)
print len(tot_set)
