function [net, info] = cnn_sketch(varargin)
addpath(genpath('/users/koustav.ghosal/koustav_ECCV/zero-shot-learning/Tools/Matconvnet/'));
run('/users/koustav.ghosal/koustav_ECCV/zero-shot-learning/Tools/Matconvnet/matlab/vl_setupnn.m') ;

opts.expDir = fullfile('/users/koustav.ghosal/koustav_ECCV/Data/finetune_caltech80_vggs_bn_0.00001_128/');
opts.imdbPath = fullfile('/users/koustav.ghosal/koustav_ECCV/Data/caltech80_wordvec_ZS.mat');
opts.netPath = ('/users/koustav.ghosal/koustav_ECCV/Data/imagenet-matconvnet-vgg-s.mat');
opts.train.batchSize = 32 ;
opts.train.numEpochs = 1000;
opts.train.continue = true ;
opts.train.gpus = [1] ;
opts.cudnn = true ;
opts.train.learningRate = [0.000001 0.00002 0.001]; 
opts.train.expDir = opts.expDir ;
opts = vl_argparse(opts, varargin) ;

load(opts.imdbPath) ;
make_zero_shot_caltech;
imdb.images.set=imdb.images.sets;
net=load(opts.netPath) ;
scal = 1 ;
bias = 0.01;

net.layers{18} = [];
net.layers{19} = [];

net.layers{1,18} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(1,1,4096,300,'single'), ...
                           'biases', bias*ones(1, 300, 'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 10, ...
                           'biasesLearningRate', 20, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;

%net.layers{19} = struct('type', 'softmaxloss') ;
ly.type = 'custom' ;
    ly.forward = @nndistance_forward ;
    ly.backward = @nndistance_backward ;
     
net.layers{19} = ly ;

net = vl_simplenn_tidy(net);

[net, info] = cnn_train(net, imdb, @getBatch, ...
    opts.train, ...
    'val', find(imdb.images.set == 3)) ;


function [im_new, labels] = getBatch(imdb, batch)

meanx = 103.939;
meany = 116.779;
meanz = 123.68;
N = length(batch);
im = imdb.images.data(:,:,:,batch);
im(:,:,1,:)=im(:,:,1,:)-meanx;
im(:,:,2,:)=im(:,:,2,:)-meany;
im(:,:,3,:)=im(:,:,3,:)-meanz;
labels = single(imdb.images.wordlabels(:,:,:,batch));
im = single(im);
WH = 224;
RS = 227-WH+1;
im_new = gpuArray(zeros(WH,WH,size(im,3),N,'single')); 
for i = 1:N
    x = randperm(RS,1);
    y = randperm(RS,1);
    roll = rand();
    if roll>0.45
        tmpImg = imrotate(im(:,:,:,i),randi([-10 10],1,1),'bilinear','crop');
    else
        tmpImg = im(:,:,:,i);
    end
    im_new(:,:,:,i) = gpuArray(tmpImg(x:(x+WH-1),y:(y+WH-1),:));
    roll = rand();
    if roll>0.5
        for j=1:3
            im_new(:,:,j,i) = fliplr(im_new(:,:,j,i));
        end
    end
end