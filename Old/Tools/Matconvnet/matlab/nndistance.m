function y = nndistance(x,w,dzdy)
% --------------------------------------------------------------------

%size(x)
%size(w)
    
if nargin < 3
    
  d = x - w ;
  y = sum(sum(sum(sum(d.*d)))) ;
 % size(y)
else
  y = dzdy * 2 * (x - w) ;
  %size(dzdy)
  %size(y)
end