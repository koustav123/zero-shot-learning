function[] = calculatePrecisionRecall_nonzs(Features,Data,layer,tag)
tic;
disp('Starting Retrieval Engine: Sketch-Sketch Non Zero-shot Mode');
addpath('/home/cvit/koustav/code/Utilities/');

%Phase 1: Initialization
%Initialize the features
load(Features);
load(Data);
numClasses = 250; % TU Berlin has 250 categories

%Select the layer from the CNN to be passed into the Retrieval Engine
% 1 - fc6 Conv
% 2 - fc6 ReLU
% 3 - fc7 Conv
% 4 - fc7 ReLU

switch layer
    case '1'
        dataMat = features(1,:,:);
    case '2'
        dataMat = features(2,:,:);
    case '3'
        dataMat = features(3,:,:);
    case '4'
        dataMat = features(4,:,:);
end

% Average the 10-crop testing features into one single 512-dimensional feature
dataMat=convfeature(dataMat,'sketch');
dataMat = squeeze(dataMat);

% Initialize the labels, sets, etc.
Labels = imdb.images.labels;
sets = imdb.images.set;
Labels = single(Labels);

% Select only the testing features
tIdx = find(sets == 3);
toc;

% Phase 2: Training KNN Classifier
tic;

% Get size of the testing Data
DatasetSize = size(dataMat(tIdx,:),1);

% Sample the labels and form a historgram for recall calculation
[ImageSampleCount,ImageUniqueLabel] = hist(Labels(tIdx),unique(Labels(tIdx)));

% Perform KNN search from Testing Data -> Testing Data only.
[IDX,D] = knnsearch(dataMat(tIdx,:),dataMat(tIdx,:),...
    'k',DatasetSize,'distance','euclidean');
toc;

tic;
% Phase 3: Calculating PR curves and Confusion Matrix
QueryLabels = Labels(tIdx);

% Initialize Confusion Matrix containing all classes - 250 here
confusionMatrix = zeros(numClasses,numClasses);
PrecisionArray=[];RecallArray=[];

% Loop over all the retrievals
for i=1:size(IDX,1)
    disp(i);
	% thisLabel -> Actual Label of the query
    thisLabel = QueryLabels(i);
    
    % Correct content initialized to 0
    cc =0;
    pr = [];re=[];
    
    % Loop over all the predictions made for this sketch
    % We want to discard the first retrieval which will be the sketch itself
    % Hence, starting from 2nd retrieval
    for k =2:size(IDX,2)    
        % If the prediction is correct, increment cc
        if (Labels(tIdx(IDX(i,k))) == thisLabel)
            cc=cc+1;
        end
        % Correct the indices
        j = k-1;
        pr(j) = cc/j;re(j) = cc/ImageSampleCount(find(thisLabel == ImageUniqueLabel));
    end
    
    % Do a 14-NN classification of the retrieval
    predictedLabel = mode(Labels(tIdx(IDX(i,2:15))));
    
    % Add the prediction to the Confusion Matrix and the PR-array
    confusionMatrix(thisLabel, predictedLabel) = confusionMatrix(thisLabel, predictedLabel) +1 ;
    PrecisionArray(i,:) = pr; RecallArray(i,:) = re;
end

%Average over all the output values
Precision = mean(PrecisionArray,1);Recall = mean(RecallArray,1);
toc;

%Save the outputs into the PR Files
save(['/home/cvit/koustav/Ameya/Sketch_Sketch_Retrieval/PR_Curves/' 'PrecisionRecall_' tag '.mat'],'Precision','Recall');
save(['/home/cvit/koustav/Ameya/Sketch_Sketch_Retrieval/PR_Curves/' 'ConfusionMatrix_' tag '.mat'],'confusionMatrix');