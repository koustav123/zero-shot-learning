#!/bin/bash
#SBATCH -A cvit
#SBATCH -n 4
#SBATCH -p long
#SBATCH --mem=25000
#SBATCH -t 24:00:00

echo $1 $2 $3 $4
/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "calculatePrecisionRecall_nonzs('$1','$2','$3','$4');"

