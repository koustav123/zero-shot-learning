figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUB_nonzs_wvec311ft590_layer4.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUB_nonzs_wvec311ft590_layer2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUB_nonzs_250softmax_layer4.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUB_nonzs_250softmax_layer2.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold on;
title('Sketch-Sketch Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR - ReLU 2','DFSR - ReLU 1','Softmax - ReLU 2','Softmax - ReLU 1');
print('sketchsketchbench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -osketchsketchbench.png sketchsketchbench.eps');