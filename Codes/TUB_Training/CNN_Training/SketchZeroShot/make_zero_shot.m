%ZS contains the zero-shot classes. We load ZS
ZS = imdb.images.ZS;

%Initialize j and set_no.
%set_no is the set to select. set_1 is the first 50 unknown, set2 is the second 50 unknown, etc.
j=1;
set_no = 1;
ZS_set=zeros(1,20000);

%Loop over all classes
for i=1:250
    disp(i);
    
    %If the class is to be excluded from training, then label all the sets in this class to be 0
    %Since our labels are in numeric order, 80 for each class, 1-250 in the order, we are hardcoding this part
    %Should ideally be generalized and written
    if (ZS(set_no,i)==0)
        for k=j:j+79
            ZS_set(1,k)=0;
        end
        j=j+80;

    %j is the pointer to the next set value in the entire dataset
    %If the class is to be included in training, then pass on the same set as in Hospedeles etal.'s dataset.
    %Since our labels are in numeric order, 80 for each class, 1-250 in the order, we are hardcoding this part
    %Should ideally be generalized and written
    elseif ZS(set_no,i)==1
        for k=j:j+79
            ZS_set(1,k)=imdb.images.set(1,k);
        end
        j=j+80;
    end
end

%Change the imdb's set to old_set and imdb's set to the new zero-shot set that we form
imdb.images.old_set = imdb.images.set;
imdb.images.set = ZS_set;
