function [net, info] = cnn_train_wordvec_sketch_full(varargin)

%Load the Matconvnet paths. We are using Matconvnet version XXXX as included along with the package for help
addpath(genpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/'));
run('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab/vl_setupnn.m');

%Load the experiment directory (where your networks will be stored), imdbPath (where your data is located) and netPath (where the intialization network is present)
opts.expDir = fullfile('../../../Data/SBIR/TUB_wordvec_0.01_128_demo/') ;
opts.imdbPath = fullfile('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/wordvec_TUB/TUB_morphed_wordvec_imdb.mat');
opts.netPath = ('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/model_without_order_info_256.mat');

%Adjust the parameters such as Batchsize, Number of Epochs, learningRate, etc. We are using GPUs for training with CuDNNv4 and CUDA 7.5
%We currently haven't coded early stopping. We have to manually stop the learning

opts.train.batchSize = 128 ;
opts.train.numEpochs = 1000;
opts.train.continue = true ;
opts.train.gpus = [1] ;
opts.cudnn = true ;
opts.train.learningRate = [0.00001*ones(1,4) 0.01*ones(1,6) 0.05] ; 
opts.train.expDir = opts.expDir ;
opts = vl_argparse(opts, varargin) ;

%Load data.
load(opts.imdbPath) ;

%Load the pre-network to get a good intialization to save time.
load(opts.netPath) ;
scal = 1 ;
bias = 0.01;

%We replace the last conv layer and the loss function to a 512x300 and then an L2 norm from the wordvectors
net.layers{20} = [];
net.layers{21} = [];
net.layers{20} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(1,1,512,300,'single'), ...
                           'biases', bias*ones(1, 300, 'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
ly.type = 'custom' ;
    ly.forward = @nndistance_forward ;
    ly.backward = @nndistance_backward ;
net.layers{21} = ly ;

%Since some networks are trained in the previous matconvnet formats, it's better to convert it to newer matconvnet version to avoid errors
net = vl_simplenn_tidy(net);

%In the following code we are following the same code as Hospedeles etal.
[net, info] = cnn_train(net, imdb, @getBatch, ...
    opts.train, ...
    'val', find(imdb.images.set == 3)) ;


function [im, labels] = getBatch(imdb, batch)

%We load the image batch along with the size and wordvector labels
N = length(batch);
im2 = imdb.images.data(:,:,6,batch) ;
im = zeros(256,256,1,N,'uint8');
labels = imdb.images.wordvec_labels(:,:,:,batch);

%Convert the logical values into uint8 values and invert the background
im = uint8(255*im2(:,:,:,:));

%We have hardcoded to pass the batch into the GPU
%CPU training is possible but not recommended
%Our model can be trained on normal laptop GPUs as well.
im = gpuArray(single(im));
im = 255 - im;

%In the following code we are following the same code as Hospedeles etal.
WH = 225;
RS = 256-WH+1;
im_new = gpuArray(zeros(WH,WH,size(im,3),N,'single')); 
for i = 1:N
    x = randperm(RS,1);
    y = randperm(RS,1);
    roll = rand();
    if roll>0.45
        tmpImg = imrotate(im(:,:,:,i),randi([-10 10],1,1),'bilinear','crop');
    else
        tmpImg = im(:,:,:,i);
    end
    
    im_new(:,:,:,i) = tmpImg(x:(x+WH-1),y:(y+WH-1),:);
    roll = rand();
    if roll>0.5
            im_new(:,:,1,i) = fliplr(im_new(:,:,1,i));
    end
end
im = im_new;


