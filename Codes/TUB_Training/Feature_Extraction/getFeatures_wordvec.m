function [features] = getFeatures(modelfilename,layers)

%Load the Matconvnet paths. We are using Matconvnet version XXXX as included along with the package for help
addpath(genpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/'));
run('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab/vl_setupnn.m');

%Load the model from which you wish to extract features
load(modelfilename);

%Load dataset from which we will extract features
load('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Data/TUB_dataset_with_order_256_wordvec.mat');

tic;
net = vl_simplenn_move(net,'gpu') ;

%Optional, uncomment if required.
%net = vl_simplenn_tidy(net);
%net.layers{end} = 'softmax';

%Phase 1: Calculate the featuresizes etc.

%Initialize values
numImages = size(imdb.images.data,4);
testY = imdb.images.wordvec_labels(:,:,:,:);
sizeLabels=size(testY);

%Pass a random sketch as the input
testimg = imdb.images.data(:,:,:,1);
testimg = uint8(255*testimg);
testimg = 255-testimg;

%Perform 10-crop testing
im1 = testimg(1:225,1:225,:) ;
im2 = testimg(32:256,1:225,:) ;
im3 = testimg(1:225,32:256,:) ;
im4 = testimg(32:256,32:256,:) ;
im5 = testimg(16:240,16:240,:) ;
im15 = cat(4,im1,im2,im3,im4,im5);
im_new = cat(4,im15,fliplr(im15));

%Hardcoded into GPU again
im_new = gpuArray(single(im_new));

%The wordvector forward pass needs this. Initialize randomly to zero.
net.layers{end}.class = zeros(sizeLabels(1),sizeLabels(2),sizeLabels(3),10);
net.layers{end}.w = zeros(sizeLabels(1),sizeLabels(2),sizeLabels(3),10);

%Forward-pass the image through the model
res = vl_simplenn(net, im_new, [], [], ...
                  'mode', 'test', ...
                  'conserveMemory', false, ...
                  'sync', true, ...
                  'cudnn', true) ;

%Determine the feature sizes              
featureSize = size(res(layers(1)).x,3);
noImgs = size(res(layers(1)).x,4);
numLayers = size(layers,2);

%Initialize the feature vectors and outputs
features = zeros(numLayers,numImages,featureSize*noImgs);
predY = zeros(1,length(testY));

%Loop over all the testing data
for i = 1:length(testY)
    %Display only significant progress
    if mod(i,1000) == 0
        disp(i);
    end
    
    %For the respective data, load a test image
    test = imdb.images.data(:,:,:,i);
    test = uint8(255*test);
    testX = 255-test;

    %Perform 10-crop testing
    im1 = testX(1:225,1:225,:) ;
    im2 = testX(32:256,1:225,:) ;
    im3 = testX(1:225,32:256,:) ;
    im4 = testX(32:256,32:256,:) ;
    im5 = testX(16:240,16:240,:) ;
    im15 = cat(4,im1,im2,im3,im4,im5);
    im_new = cat(4,im15,fliplr(im15));
    
    %Load it in GPU
    im_new = gpuArray(single(im_new));
    
    %The wordvector forward pass needs this. Initialize randomly to zero.
    net.layers{end}.class = zeros(sizeLabels(1),sizeLabels(2),sizeLabels(3),10);
    net.layers{end}.w = zeros(sizeLabels(1),sizeLabels(2),sizeLabels(3),10);

    %Forward-pass the image through the model
    res = vl_simplenn(net, im_new, [], [], ...
                      'mode', 'test', ...
                      'conserveMemory', false, ...
                      'sync', true, ...
                      'cudnn', true) ;
    
    %Check this shit. This can ruin entire set of experiments.
    for j = 1:numLayers
        feature = squeeze(gather(res(layers(j)).x));
        feature = reshape(feature,1,[]);
        features(j,i,:) = squeeze(feature);
    end
end

toc;


