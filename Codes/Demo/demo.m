function [] = demo(sketchfilename,numImagestobeRetrieved)

%Load the Matconvnet paths. We are using Matconvnet version XXXX as included along with the package for help
addpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab');
addpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/examples');
addpath('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/');
run('/media/ameya/Linux/Awesome_Stuff/Projects/ECCV/Tools/Matconvnet/matlab/vl_setupnn.m');

%Load the Data
%
% wordvec_demo_epoch495.mat - Trained network with Word2vec on non-temporal sketches
% ImageTestFeaturesProjected - Feature storage for the images in caltech-256
% SketchWeightMatrix and ImageWeightMatrix - Used to project any new sketch or image features extracted
% Reversemapping - Mapping from IDX to original indices to retrieve images
% Caltech_names - Mapping from original indices to images

load('../Data/Demo/wordvec_demo_epoch495.mat');
load('../Data/Demo/ImageTestFeaturesProjected.mat');
load('../Data/Demo/SketchWeightMatrix.mat');
load('../Data/Demo/reversemapping.mat');
load('../Data/Demo/caltech_names.mat');
load('../Data/Demo/ImageWeightMatrix.mat');
net = vl_simplenn_move(net,'gpu') ;

tic;

% We are using ReLU2
layer = 19;

% Number of images in the dataset
DatasetSize = size(ImageSampleFeaturesProjected,1);

%Load the image and preprocess it
im = imread(sketchfilename);
im = imresize(im,[256 256]);
se = strel('disk',5);
im=imerode(im,se,'same');
level = graythresh(im);
im = im2bw(im,level);

%Do 10-crop testing
testimg = im;
testimg = uint8(255*testimg);
testimg = 255-testimg;
im1 = testimg(1:225,1:225,:) ;
im2 = testimg(32:256,1:225,:) ;
im3 = testimg(1:225,32:256,:) ;
im4 = testimg(32:256,32:256,:) ;
im5 = testimg(16:240,16:240,:) ;
im15 = cat(4,im1,im2,im3,im4,im5);
im_new = cat(4,im15,fliplr(im15));
im_new = gpuArray(single(im_new));

% Normal stuff
net.layers{end}.class = zeros(1,1,300,10);
net.layers{end}.w = zeros(1,1,300,10);

% Forward pass the model and get all features from all layers
res = vl_simplenn(net, im_new, [], [], ...
                  'mode', 'test', ...
                  'conserveMemory', false, ...
                  'sync', true, ...
                  'cudnn', true) ;

%
feature = squeeze(gather(res(layer).x));
feature = mean(feature(:,:),2);
features(1,:) = squeeze(feature);

SketchFeatureProjected = (Wx' * features')';

%Now we have SketchFeaturesProjected and ImageFeaturesProjected. We can simply run KNN and get the retrievals
[IDX,D] = knnsearch(single(ImageSampleFeaturesProjected),SketchFeatureProjected,'k',DatasetSize,'Distance','euclidean');
toc;
% Display the results obtained from sketches
% TODO: Add an GUI interface
disp(sketchfilename);
for i=1:numImagestobeRetrieved
	disp(names(imdbidxtest(IDX(1,i))));
end
