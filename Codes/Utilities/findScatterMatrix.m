function [ SM ] = findScatterMatrix( X,XLabels,Y,YLabels,type )
%FINDSCATTERMATRIX Summary of this function goes here
%   Detailed explanation goes here
SM = zeros(size(X,2));
Classes = unique(XLabels);
switch type
    case 'intra'
        for c =1:numel(Classes)
            idx_1 = (XLabels == Classes(c));
            idx_2 = (YLabels == Classes(c));
            X_t1 = X(idx_1,:);
            Y_t1 = Y(idx_2,:);
            samples = [X_t1(1:end,:); Y_t1(1:end,:)];
            labels = [Classes(c)*ones(1,size(samples,1))];
            SM = SM + samples'*samples;
        end
        SM = SM/length(Classes);
        
    case 'inter'
        mG = mean([X;Y] ,1);
        for c =1:numel(Classes)
            idx_1 = (XLabels == Classes(c));
            idx_2 = (YLabels == Classes(c));
            X_t1 = X(idx_1,:);
            Y_t1 = Y(idx_2,:);
            samples = [X_t1(1:end,:); Y_t1(1:end,:)];
            mL = mean(samples,1);
            SM = SM + size(samples,1)*(mL - mG)'*(mL - mG);
           
        end
        
        SM = SM/length(Classes);
        
end
end

