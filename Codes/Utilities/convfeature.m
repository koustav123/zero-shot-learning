function [feature] = convfeature(inp_feature,type)

if strcmp(type,'sketch')
    size_feature = size(inp_feature);
    feature = zeros(size_feature(1),size_feature(2),512);
    for i=1:10
       feature = bsxfun(@plus,feature,inp_feature(:,:,(i-1)*512+1:i*512)); 
    end
    feature = bsxfun(@rdivide,feature,10);
end
if strcmp(type,'image')
    size_feature = size(inp_feature);
    feature = zeros(size_feature(1),size_feature(2),4096);
    for i=1:2
       feature = bsxfun(@plus,feature,inp_feature(:,:,(i-1)*4096+1:i*4096)); 
    end
    feature = bsxfun(@rdivide,feature,2);
end