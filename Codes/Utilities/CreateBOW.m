function [feature] = CreateBow(path,k);
data = load(path);data=double(data.h);
[feature,indices] = hist(data,[1:k]);
end