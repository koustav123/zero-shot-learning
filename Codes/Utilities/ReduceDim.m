function [ ReducedData ] = ReduceDim( X,dim,step )
%REDUCEDIM Summary of this function goes here
%   Detailed explanation goes here

basis = pca(X(1:step:end,:));
ReducedData = X*basis(:,1:dim);
end

