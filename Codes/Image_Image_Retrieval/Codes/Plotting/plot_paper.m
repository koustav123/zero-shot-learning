figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Image_Image_Retrieval/PR_Curves/PrecisionRecall_Caltech_nonzs_wvec_layer4.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Image_Image_Retrieval/PR_Curves/PrecisionRecall_Caltech_nonzs_wvec_layer2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Image_Image_Retrieval/PR_Curves/PrecisionRecall_Caltech_nonzs_sftmx13_layer4.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Image_Image_Retrieval/PR_Curves/PrecisionRecall_Caltech_nonzs_sftmx13_layer2.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold on;
title('Image-Image Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(VGG-S) - ReLU 2','DFSR(VGG-S) - ReLU 1','Softmax(VGG-S) - ReLU 2','Softmax(VGG-S) - ReLU 1');
print('imageimagebench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -oimageimagebench.png imageimagebench.eps');