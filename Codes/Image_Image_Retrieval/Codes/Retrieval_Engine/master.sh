#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_vggs.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 4 Caltech_nonzs_sftmx_layer4
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_vggs.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 3 Caltech_nonzs_sftmx_layer3
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_vggs.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 2 Caltech_nonzs_sftmx_layer2
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_vggs.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 1 Caltech_nonzs_sftmx_layer1

#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 4 Caltech_nonzs_wvec_layer4
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 3 Caltech_nonzs_wvec_layer3
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 2 Caltech_nonzs_wvec_layer2
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 1 Caltech_nonzs_wvec_layer1

sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 4 Caltech_nonzs_sftmx13_layer4
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 3 Caltech_nonzs_sftmx13_layer3
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 2 Caltech_nonzs_sftmx13_layer2
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/imdb/caltech_80_wordvec_ZS.mat 1 Caltech_nonzs_sftmx13_layer1

