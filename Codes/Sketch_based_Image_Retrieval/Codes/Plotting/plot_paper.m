figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Sketch_based_Image_Retrieval/PR_Curves/PrecisionRecall_WVEC79_WVEC590_4_4.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Sketch_based_Image_Retrieval/PR_Curves/PrecisionRecall_WVEC79_WVEC590_2_2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Sketch_based_Image_Retrieval/PR_Curves/PrecisionRecall_SFTMX13_SFTMX250_4_4.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Sketch_based_Image_Retrieval/PR_Curves/PrecisionRecall_SFTMX13_SFTMX250_2_2.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold on;
title('Sketch based Image Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR - ReLU 2','DFSR - ReLU 1','Softmax - ReLU 2','Softmax - ReLU 1');
print('sketchimagecrossbench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -osketchimagecrossbench.png sketchimagecrossbench.eps');