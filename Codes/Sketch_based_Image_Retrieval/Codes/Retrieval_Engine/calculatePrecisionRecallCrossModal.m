function [  ] = calculatePrecisionRecallCrossModal(imageFeatures,sketchFeatures,sketchlayer,imagelayer,tag)
%FN_CALCULATEPRECISIONRECALLCROSSMODAL Summary of this function goes here
%   Detailed explanation goes here
%

%Phase 1: Intialization
tic;
disp('Starting Retrieval Engine: Sketch based Image Retrieval- Non Zero-shot Mode');
% Add utility scripts
addpath('/home/cvit/koustav/code/Utilities/');

% Load data - Mapping, Image and sketch features, labels, sets etc 
mapping = importdata('/lustre/koustav.ghosal/ZeroShot/mapping/mapping_CCA.txt');
FullImageFeat = load(imageFeatures);
FullSketchFeat = load(sketchFeatures);

OldSketchLabels = load('/lustre/koustav.ghosal/ZeroShot/imdb/SketchLabels.mat');
OldImageLabels = load('/lustre/koustav.ghosal/ZeroShot/imdb/ImageLabels.mat');
OldSketchLabels = OldSketchLabels.SketchLabels;
OldImageLabels = OldImageLabels.ImageLabels;
SketchData.imdb.images.labels = OldSketchLabels;
ImageData.imdb.images.labels = OldImageLabels;

SketchSet = load('/lustre/koustav.ghosal/ZeroShot/imdb/SketchSet.mat');
ImageSet = load('/lustre/koustav.ghosal/ZeroShot/imdb/ImageSet.mat');
SketchData.imdb.images.set = SketchSet.SketchSet;
ImageData.imdb.images.set = ImageSet.ImageSet;
imagelayer = str2num(imagelayer);
sketchlayer = str2num(sketchlayer);

%For demo purposes

imdbidx = ones(1,20560);
for i=1:20560
    imdbidx(1,i)=i*imdbidx(1,i);
end
% Calculate the average feature - both 4096 dimensional for images and
% 512 dimensional for sketches
FullImageFeat.features_caltech = convfeature(FullImageFeat.features_caltech,'image');
FullSketchFeat.features = convfeature(FullSketchFeat.features,'sketch');
toc;

% Phase 2: Map the data and form the correspondences between classes
tic;

%Initialize data
LabelMap = mapping.data;
ImageFeaturesTrain = [];
ImageFeaturesTest = [];
SketchFeaturesTest = [];
SketchFeaturesTrain = [];
NewImageLabelsTrain = [];
NewImageLabelsTest = [];
NewSketchLabelsTest = [];
NewSketchLabelsTrain = [];
imdbidxtest = [];
% Loop for all the 105 corresponding classes
for i = 1:105
    disp(i);
    % For debugging purposes - TBD [Make into an assert statement for final release]
    %currentImageLabel = LabelMap(i,1);
    %currentSketchLabel = LabelMap(i,2);
    
    % Find the indexes of training and testing data in both images and sketches
    % Training -> set == 1 and Testing -> set == 3
    % Also, we want labels of only the selected class in question
    
    idx_images_test = find(ImageData.imdb.images.labels == LabelMap(i,1)...
        & ImageData.imdb.images.set == 3 );
    idx_images_train = find(ImageData.imdb.images.labels == LabelMap(i,1) ...
        & ImageData.imdb.images.set == 1 );
    
    idx_sketches_train = find(SketchData.imdb.images.labels == ...
        LabelMap(i,2) & SketchData.imdb.images.set == 1);
    idx_sketches_test = find(SketchData.imdb.images.labels == ...
        LabelMap(i,2) & SketchData.imdb.images.set == 3);
    
    ImageFeaturesTrain = [ImageFeaturesTrain ;...
        squeeze(FullImageFeat.features_caltech(imagelayer,idx_images_train,:))];
    ImageFeaturesTest = [ImageFeaturesTest ;...
        squeeze(FullImageFeat.features_caltech(imagelayer,idx_images_test,:))];
    
    SketchFeaturesTest = [SketchFeaturesTest ;...
        squeeze(FullSketchFeat.features(sketchlayer,idx_sketches_test,:))];
    SketchFeaturesTrain = [SketchFeaturesTrain ;...
        squeeze(FullSketchFeat.features(sketchlayer,idx_sketches_train,:))];
    
    % Create new labels for the CCA module
    imdbidxtest = [imdbidxtest idx_images_test];
    NewImageLabelsTrain = [NewImageLabelsTrain ; i*ones(size(idx_images_train,2),1)];
    NewImageLabelsTest = [NewImageLabelsTest ; i*ones(size(idx_images_test,2),1)];
    NewSketchLabelsTest = [NewSketchLabelsTest ; i*ones(size(idx_sketches_test,2),1)];
    NewSketchLabelsTrain = [NewSketchLabelsTrain ; i*ones(size(idx_sketches_train,2),1)];
end
toc;
save('reversemapping.mat','imdbidxtest');
% Phase 3 - Training Cluster-CCA
tic;

% Experiment #1 PCA - *Doesn't work*. Reason- Doesn't preserve the learned mapping.
% We would require to use some clustered joint-dimension reduction
% technique

%SketchFeaturesTrain = ReduceDim(SketchFeaturesTrain,100,1);
%SketchFeaturesTest = ReduceDim(SketchFeaturesTest,100,1);
%ImageFeatures = ReduceDim(ImageFeatures,100,1);
toc;

tic;

% Experiment #2 Clustered CCA - Give the features to the cluster-CCA module
[Wx,Wy]=clusterCCA_1(SketchFeaturesTrain,ImageFeaturesTrain,...
    NewSketchLabelsTrain,NewImageLabelsTrain);
toc;

% Phase 4 - Testing the cluster-CCA
tic;

% Project the learned features using the Wx and Wy matrices into the
% subspace
SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
ImageSampleFeaturesProjected = (Wy' * ImageFeaturesTest')';

% Run the retrieval module to get the retrieval results
[Precision, Recall, confusionMatrix] = retrieval(ImageSampleFeaturesProjected,...
    NewImageLabelsTest,SketchTestFeaturesProjected,NewSketchLabelsTest);

save('SketchWeightMatrix.mat','Wx');
save('ImageWeightMatrix.mat','Wy');
save('ImageTestFeaturesProjected.mat','ImageSampleFeaturesProjected');

save(['/home/cvit/koustav/Ameya/Sketch_based_Image_Retrieval/PR_Curves/' 'PrecisionRecall_' tag '.mat'],'Precision','Recall');
save(['/home/cvit/koustav/Ameya/Sketch_based_Image_Retrieval/PR_Curves/' 'ConfusionMatrix_' tag '.mat'],'confusionMatrix');
toc;

