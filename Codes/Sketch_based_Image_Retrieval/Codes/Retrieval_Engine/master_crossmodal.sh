sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_softmax_250.mat 1 1 SFTMX13_SFTMX250_1_1
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_softmax_250.mat 2 2 SFTMX13_SFTMX250_2_2
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_softmax_250.mat 3 3 SFTMX13_SFTMX250_3_3
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_softmax_250.mat 4 4 SFTMX13_SFTMX250_4_4

sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_mine_epoch590ft311.mat 1 1 SFTMX13_WVEC590_1_1
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_mine_epoch590ft311.mat 2 2 SFTMX13_WVEC590_2_2
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_mine_epoch590ft311.mat 3 3 SFTMX13_WVEC590_3_3
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_softmax_epoch13.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_mine_epoch590ft311.mat 4 4 SFTMX13_WVEC590_4_4

sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_softmax_250.mat 1 1 WVEC79_SFTMX250_1_1
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_softmax_250.mat 2 2 WVEC79_SFTMX250_2_2
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_softmax_250.mat 3 3 WVEC79_SFTMX250_3_3
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_softmax_250.mat 4 4 WVEC79_SFTMX250_4_4

sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_mine_epoch590ft311.mat 1 1 WVEC79_WVEC590_1_1
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_mine_epoch590ft311.mat 2 2 WVEC79_WVEC590_2_2
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_mine_epoch590ft311.mat 3 3 WVEC79_WVEC590_3_3
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_mine_epoch590ft311.mat 4 4 WVEC79_WVEC590_4_4

#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_311finetune_398epoch_WordVec_512_4layers_features.mat 1 1 WVEC79_WVEC398_1_1
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_311finetune_398epoch_WordVec_512_4layers_features.mat 2 2 WVEC79_WVEC398_2_2
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_311finetune_398epoch_WordVec_512_4layers_features.mat 3 3 WVEC79_WVEC398_3_3
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_finetune_epoch79.mat /lustre/koustav.ghosal/ZeroShot/Features/TUB_311finetune_398epoch_WordVec_512_4layers_features.mat 4 4 WVEC79_WVEC398_4_4