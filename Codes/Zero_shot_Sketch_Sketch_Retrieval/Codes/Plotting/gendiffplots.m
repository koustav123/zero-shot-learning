conf_wvec = load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/ConfusionMatrix_TUBZS_WVEC_set5_layer1.mat');
conf_softmax = load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/ConfusionMatrix_TUBZS_SFTMX_set5_layer1.mat');
setno = 5;
labels = {'airplane','alarm clock','angel','ant','apple','arm','armchair','ashtray','axe','backpack','banana','barn','baseball bat','basket','bathtub','bear (animal)','bed','bee','beer-mug','bell','bench','bicycle','binoculars','blimp','book','bookshelf','boomerang','bottle opener','bowl','brain','bread','bridge','bulldozer','bus','bush','butterfly','cabinet','cactus','cake','calculator','camel','camera','candle','cannon','canoe','car (sedan)','carrot','castle','cat','cell phone','chair','chandelier','church','cigarette','cloud','comb','computer monitor','computer-mouse','couch','cow','crab','crane (machine)','crocodile','crown','cup','diamond','dog','dolphin','donut','door','door handle','dragon','duck','ear','elephant','envelope','eye','eyeglasses','face','fan','feather','fire hydrant','fish','flashlight','floor lamp','flower with stem','flying bird','flying saucer','foot','fork','frog','frying-pan','giraffe','grapes','grenade','guitar','hamburger','hammer','hand','harp','hat','head','head-phones','hedgehog','helicopter','helmet','horse','hot air balloon','hot-dog','hourglass','house','human-skeleton','ice-cream-cone','ipod','kangaroo','key','keyboard','knife','ladder','laptop','leaf','lightbulb','lighter','lion','lobster','loudspeaker','mailbox','megaphone','mermaid','microphone','microscope','monkey','moon','mosquito','motorbike','mouse (animal)','mouth','mug','mushroom','nose','octopus','owl','palm tree','panda','paper clip','parachute','parking meter','parrot','pear','pen','penguin','person sitting','person walking','piano','pickup truck','pig','pigeon','pineapple','pipe (for smoking)','pizza','potted plant','power outlet','present','pretzel','pumpkin','purse','rabbit','race car','radio','rainbow','revolver','rifle','rollerblades','rooster','sailboat','santa claus','satellite','satellite dish','saxophone','scissors','scorpion','screwdriver','sea turtle','seagull','shark','sheep','ship','shoe','shovel','skateboard','skull','skyscraper','snail','snake','snowboard','snowman','socks','space shuttle','speed-boat','spider','sponge bob','spoon','squirrel','standing bird','stapler','strawberry','streetlight','submarine','suitcase','sun','suv','swan','sword','syringe','t-shirt','table','tablelamp','teacup','teapot','teddy-bear','telephone','tennis-racket','tent','tiger','tire','toilet','tomato','tooth','toothbrush','tractor','traffic light','train','tree','trombone','trousers','truck','trumpet','tv','umbrella','van','vase','violin','walkie talkie','wheel','wheelbarrow','windmill','wine-bottle','wineglass','wrist-watch','zebra'};
diff_conf = conf_wvec.confusionMatrix - conf_softmax.confusionMatrix;

figure;

for i=(setno-1)*50+1:setno*50
    valid=find(diff_conf(i,:)~=0);
    if(~isempty(valid))
    bar(diff_conf(i,valid));ylim([-26 26]);
    set(gca,'XTickLabel',labels(valid));
    title(['Class Retrieved: ' labels(i)]);
    saveas(gcf,['/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/Diff_Plots/Diff_Conf_set5_class' num2str(i) '_wvec_sftmx'],'pdf')
    end
end

conf_wvec = load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/ConfusionMatrix_TUBZS_WVEC_set4_layer1.mat');
conf_softmax = load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/ConfusionMatrix_TUBZS_SFTMX_set4_layer1.mat');
setno = 4;
diff_conf = conf_wvec.confusionMatrix - conf_softmax.confusionMatrix;

for i=(setno-1)*50+1:setno*50
    valid=find(diff_conf(i,:)~=0);
    if(~isempty(valid))
    bar(diff_conf(i,valid));ylim([-26 26]);
    set(gca,'XTickLabel',labels(valid));
    title(['Class Retrieved: ' labels(i)]);
    saveas(gcf,['/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/Diff_Plots/Diff_Conf_set4_class' num2str(i) '_wvec_sftmx'],'pdf')
    end
end

conf_wvec = load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/ConfusionMatrix_TUBZS_WVEC_set3_layer1.mat');
conf_softmax = load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/ConfusionMatrix_TUBZS_SFTMX_set3_layer1.mat');
setno = 3;
diff_conf = conf_wvec.confusionMatrix - conf_softmax.confusionMatrix;

for i=(setno-1)*50+1:setno*50
    valid=find(diff_conf(i,:)~=0);
    if(~isempty(valid))
    bar(diff_conf(i,valid));ylim([-26 26]);
    set(gca,'XTickLabel',labels(valid));
    title(['Class Retrieved: ' labels(i)]);
    saveas(gcf,['/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/Diff_Plots/Diff_Conf_set3_class' num2str(i) '_wvec_sftmx'],'pdf')
    end
end

conf_wvec = load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/ConfusionMatrix_TUBZS_WVEC_set2_layer1.mat');
conf_softmax = load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/ConfusionMatrix_TUBZS_SFTMX_set2_layer1.mat');
setno = 2;
diff_conf = conf_wvec.confusionMatrix - conf_softmax.confusionMatrix;

for i=(setno-1)*50+1:setno*50
    valid=find(diff_conf(i,:)~=0);
    if(~isempty(valid))
    bar(diff_conf(i,valid));ylim([-26 26]);
    set(gca,'XTickLabel',labels(valid));
    title(['Class Retrieved: ' labels(i)]);
    saveas(gcf,['/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/Diff_Plots/Diff_Conf_set2_class' num2str(i) '_wvec_sftmx'],'pdf')
    end
end

