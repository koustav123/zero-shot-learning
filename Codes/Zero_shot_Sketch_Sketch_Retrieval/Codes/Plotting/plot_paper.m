figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set5_layer1.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set5_layer2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_SFTMX_set5_layer1.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_SFTMX_set5_layer2.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch-Sketch Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - Conv 1','DFSR(ZS) - ReLU 1','Softmax(ZS) - Conv 1','Softmax(ZS) - ReLU 1');
print('zeroshotsketchsketchset5bench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ozeroshotsketchsketchset5bench.png zeroshotsketchsketchset5bench.eps');

figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set4_layer1.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set4_layer2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_SFTMX_set4_layer1.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_SFTMX_set4_layer2.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch-Sketch Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - Conv 1','DFSR(ZS) - ReLU 1','Softmax(ZS) - Conv 1','Softmax(ZS) - ReLU 1');
print('zeroshotsketchsketchset4bench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ozeroshotsketchsketchset4bench.png zeroshotsketchsketchset4bench.eps');

figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set3_layer1.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set3_layer2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_SFTMX_set3_layer1.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_SFTMX_set3_layer2.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch-Sketch Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - Conv 1','DFSR(ZS) - ReLU 1','Softmax(ZS) - Conv 1','Softmax(ZS) - ReLU 1');
print('zeroshotsketchsketchset3bench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ozeroshotsketchsketchset3bench.png zeroshotsketchsketchset3bench.eps');

figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set2_layer1.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set2_layer2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_SFTMX_set2_layer1.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_SFTMX_set2_layer2.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch-Sketch Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - Conv 1','DFSR(ZS) - ReLU 1','Softmax(ZS) - Conv 1','Softmax(ZS) - ReLU 1');
print('zeroshotsketchsketchset2bench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ozeroshotsketchsketchset2bench.png zeroshotsketchsketchset2bench.eps');


figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set5_layer1.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set5_layer2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set5_layer3.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_set5_layer4.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch-Sketch Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - Conv 1','DFSR(ZS) - ReLU 1','DFSR(ZS) - Conv 2','DFSR(ZS) - ReLU 2');
print('sketchsketchzsvarlayersDFSRbench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -osketchsketchzsvarlayersDFSRbench.png sketchsketchzsvarlayersDFSRbench.eps');

figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_FULL_set5_layer1.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_FULL_set5_layer2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_FULL_set5_layer3.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/PrecisionRecall_TUBZS_WVEC_FULL_set5_layer4.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch-Sketch Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(Full) - Conv 1','DFSR(Full) - ReLU 1','DFSR(Full) - Conv 2','DFSR(Full) - ReLU 2');
print('sketchsketchzsvarlayersDFSRbench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -osketchsketchzsvarlayersDFSRbench.png sketchsketchzsvarlayersDFSRbench.eps');



