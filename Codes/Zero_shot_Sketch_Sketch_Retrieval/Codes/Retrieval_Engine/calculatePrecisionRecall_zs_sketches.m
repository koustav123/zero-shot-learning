function[] = calculatePrecisionRecall_zs_sketches(Features,Data,setno, layer,tag)

% Phase 1 - Initializations
addpath('~/code/Utilities/');

load(Features);
load(Data);

% Average the 10-crop testing features into one single 512-dimensional feature
features = convfeature(features,'sketch');
setno = str2num(setno);
%Select the layer from the CNN to be passed into the Retrieval Engine
% 1 - fc6 Conv
% 2 - fc6 ReLU
% 3 - fc7 Conv
% 4 - fc7 ReLU

switch layer
    case '1'
        dataMat = features(1,:,:);
    case '2'
        dataMat = features(2,:,:);
    case '3'
        dataMat = features(3,:,:);
    case '4'
        dataMat = features(4,:,:);
end

% Form the Zero-shot set vector
ZS = imdb.images.ZS;

j=1;
ZS_set=zeros(1,20000);

% Loop over each class
for i=1:250
    disp(i);
    
    % If the class was trained with, exclude it from the retrieval
    if (ZS(setno,i)==1)
        for k=j:j+79
            ZS_set(1,k)=0;
        end
        j=j+80;
        
    % If the class was never seen, i.e. a zero-shot class, then retain the
    % train-test split as designed by the Hospedeles group.
    elseif ZS(setno,i)==0
        for k=j:j+79
            ZS_set(1,k)=imdb.images.set(1,k);
        end
        j=j+80;
    end
    
end

% old_set -> original set representation
% sets -> new set - trained classes excluded - zeroshot representation
imdb.images.old_set = imdb.images.set;
imdb.images.set = ZS_set;

% Initialize set number, feature matrix, labels, zero-shot sets and old sets
dataMat = squeeze(dataMat);
Labels = imdb.images.labels;
sets = imdb.images.set;
old_sets = imdb.images.old_set;
Labels = single(Labels);

% tIdx -> Sketches to perform retrieval on
% fIdx -> Sketches in the database. 
% Remember, do not keep any training image the CNN has seen in the data for
% testing ever
tIdx = find(sets == 3);
fIdx = find(old_sets == 3);

% Phase 2: Training KNN Classifier
% Get size of the testing Data
DatasetSize = size(dataMat(fIdx,:),1);

% Sample the labels and form a historgram for recall calculation
[ImageSampleCount,ImageUniqueLabel] = hist(Labels(1,fIdx),unique(Labels(1,fIdx)));

% Perform KNN search from Zero-shot Testing Data -> Zero-shot Training+Testing Data
% CNN has seen neither ever, so it is safe
[IDX,D] = knnsearch(dataMat(fIdx,:),dataMat(tIdx,:),...
    'k',DatasetSize,'distance','euclidean');

% Phase 3: Calculating PR curves and Confusion Matrix
QueryLabels = Labels(tIdx);

% Initialize Confusion Matrix containing all classes - 250 here
confusionMatrix = zeros(250,250);
PrecisionArray=[];RecallArray=[];

% Loop over all the retrievals
for i=1:size(IDX,1)
    disp(i);
    
    % thisLabel -> Actual Label of the query
    thisLabel = QueryLabels(i);
    
    % Correct content initialized to 0
    cc =0;
    pr = [];re=[];
    
    % Loop over all the predictions made for this sketch
    % We want to discard the first retrieval which will be the sketch itself
    % Hence, starting from 2nd retrieval
    for k =2:size(IDX,2)
        % If the prediction is correct, increment cc
        if (Labels(fIdx(IDX(i,k))) == thisLabel)
            cc=cc+1;
        end
        % Correct the indices
        j = k-1;
        pr(j) = cc/j;re(j) = cc/ImageSampleCount(find(thisLabel == ImageUniqueLabel));
        
    end
    
    % Do a 14-NN classification of the retrieval
    predictedLabel = mode(Labels(tIdx(IDX(i,2:15))));
    
    % Add the prediction to the Confusion Matrix and the PR-array
    confusionMatrix(thisLabel, predictedLabel) = confusionMatrix(thisLabel, predictedLabel) +1 ;
    PrecisionArray(i,:) = pr; RecallArray(i,:) = re;
end

%Average over all the output values
Precision = mean(PrecisionArray,1);Recall = mean(RecallArray,1);

%Save the outputs into the PR Files
save(['/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/' 'PrecisionRecall_' tag '.mat'],'Precision','Recall');
save(['/home/cvit/koustav/Ameya/Zero_shot_Sketch_Sketch_Retrieval/PR_Curves/' 'ConfusionMatrix_' tag '.mat'],'confusionMatrix');