#import libraries
import h5py
import numpy as np
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier

Datadir = '/media/ameya/Awesomeness/ACMMM_Release/Data/'

#Disable stupid warnings!
import warnings
warnings.filterwarnings("ignore")

#Custom shuffle function
def shuffle(X, y, seed=1337):
    np.random.seed(seed)
    shuffle = np.arange(len(y))
    np.random.shuffle(shuffle)
    X = X[:,shuffle]
    y = y[shuffle]
    return X, y

print('Starting Classification Engine...')

print('Loading Data..')
#You can adjust the data path here as well.
ff = h5py.File(Datadir+'Classification/TUB_wordvecaveraged_epoch590ft311.mat','r')
fimdb = h5py.File(Datadir+'TUB_DATA_MORPHED_ZS.mat','r')
labels = fimdb.get('imdb/images/labels')
setmap = fimdb.get('imdb/images/set')
features = ff.get('features')

#Convert everything to numpy arrays
labels = np.array(labels)
setmap = np.array(setmap)
features = np.array(features)
labels = labels.flatten()
setmap = setmap.flatten()

#Create the train-test splits
train=np.array(np.nonzero(setmap==1))[0]
test=np.array(np.nonzero(setmap==3))[0]

#Layer no 1 gives best classification accuracies.
#Tried with layer 2, layer 3, layer 4 with this.
#Much better performance with layer 1.
X_train = features[:,train,0]
X_test = features[:,test,0]
Y_train = labels[train]
Y_test = labels[test]
X_train, Y_train = shuffle(X_train,Y_train)
print('Data loading complete!')
print('Preprocessing Data...')
#Scale the inputs both train and test
X_train = preprocessing.scale(X_train)
print(X_train.shape)
X_test = preprocessing.scale(X_test)
print(X_test.shape)

#Make minor adjustments to the data 
X_train=X_train.swapaxes(0,1)
X_test=X_test.swapaxes(0,1)
print('Preprocessing finished!')

#Chose Randomforest just because I have used this classifier before and training is very fast
print('Training RandomForest!')

#Tried depth = 25, 32, 50 and n_estimators = 250, 500 and 1000
#This one gave the best results
#Used a randomstate to be able to reproduce results

#This should give 70.22% accuracy for classification
clf = RandomForestClassifier(max_depth=50, n_estimators=1000, random_state=0, n_jobs=8)
clf.fit(X_train , Y_train)
print('Starting the testing...')
print(clf.score(X_test, Y_test))
print('Done !! Enjoy!')