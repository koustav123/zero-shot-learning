Datadir = '/media/ameya/Awesomeness/ACMMM_Release/Data/';
Codedir = '/media/ameya/Awesomeness/ACMMM_Release/';

%Load the Wordvec features
load([Datadir 'TUB_mine_epoch590ft311.mat']);

%Average all the outputs
addpath([Codedir 'Utilities']);
features = convfeature(features,'sketch');

%Save it as a new file
save([Datadir 'Classification/TUB_wordvecaveraged_epoch590ft311.mat'],'features','-v7.3');