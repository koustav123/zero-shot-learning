figure(1);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set5_2_2.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set5_1_1.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_SFTMX_set5_2_2.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_SFTMX_set5_1_1.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch based Image Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - ReLU 1','DFSR(ZS) - Conv 1','Softmax(ZS) - ReLU 1','Softmax(ZS) - Conv 1');
print('zeroshotsketchimagecrossmodalset5bench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ozeroshotsketchimagecrossmodalset5bench.png zeroshotsketchimagecrossmodalset5bench.eps');

figure(2);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set4_2_2.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set4_1_1.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_SFTMX_set4_2_2.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_SFTMX_set4_1_1.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch based Image Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - ReLU 1','DFSR(ZS) - Conv 1','Softmax(ZS) - ReLU 1','Softmax(ZS) - Conv 1');
print('zeroshotsketchimagecrossmodalset4bench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ozeroshotsketchimagecrossmodalset4bench.png zeroshotsketchimagecrossmodalset4bench.eps');

figure(3);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set3_2_2.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set3_1_1.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_SFTMX_set3_2_2.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_SFTMX_set3_1_1.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch based Image Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - ReLU 1','DFSR(ZS) - Conv 1','Softmax(ZS) - ReLU 1','Softmax(ZS) - Conv 1');
print('zeroshotsketchimagecrossmodalset3bench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ozeroshotsketchimagecrossmodalset3bench.png zeroshotsketchimagecrossmodalset3bench.eps');

figure(4);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set2_2_2.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set2_1_1.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_SFTMX_set2_2_2.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_SFTMX_set2_1_1.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch based Image Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - ReLU 1','DFSR(ZS) - Conv 1','Softmax(ZS) - ReLU 1','Softmax(ZS) - Conv 1');
print('zeroshotsketchimagecrossmodalset2bench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ozeroshotsketchimagecrossmodalset2bench.png zeroshotsketchimagecrossmodalset2bench.eps');


figure(5);
pos = get(gcf, 'Position');
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set5_1_1.mat');
plot(Recall, Precision,'r-');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set5_2_2.mat');
plot(Recall, Precision,'m--');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set5_3_3.mat');
plot(Recall, Precision,'k-.');axis([0.0, 1.0, 0.0, 1.0]);hold on;
load('/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/PrecisionRecall_ZS_Cross_WVEC_WVEC_set5_4_4.mat');
plot(Recall, Precision,'b:');axis([0.0, 1.0, 0.0, 1.0]);hold off;
title('Zero-shot Sketch based Image Retrieval');
xlabel('Recall');ylabel('Precision');
legend('DFSR(ZS) - Conv 1','DFSR(ZS) - ReLU 1','DFSR(ZS) - Conv 2','DFSR(ZS) - ReLU 2');
print('zeroshotsketchimagecrossmodalset5bench','-depsc2','-r300');
system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ozeroshotsketchimagecrossmodalset5bench.png zeroshotsketchimagecrossmodalset5bench.eps');



