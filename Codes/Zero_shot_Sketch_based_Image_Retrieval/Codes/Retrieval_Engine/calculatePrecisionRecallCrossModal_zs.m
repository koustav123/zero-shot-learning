function [  ] = calculatePrecisionRecallCrossModal_zs(imageFeatures,sketchFeatures,sketchlayer,imagelayer,setno, tag )
%FN_CALCULATEPRECISIONRECALLCROSSMODAL Summary of this function goes here
%   Detailed explanation goes here
%

%Phase 1: Intialization
tic;
setno = str2num(setno);
% Add utility scripts
addpath('/home/cvit/koustav/code/Utilities/');

% Load data - Mapping, Image and sketch features, labels, sets etc 
mapping = importdata('/lustre/koustav.ghosal/ZeroShot/mapping/mapping_CCA.txt');
FullImageFeat = load(imageFeatures);
FullSketchFeat = load(sketchFeatures);

OldSketchLabels = load('/lustre/koustav.ghosal/ZeroShot/imdb/SketchLabels.mat');
OldImageLabels = load('/lustre/koustav.ghosal/ZeroShot/imdb/ImageLabels.mat');
OldSketchLabels = OldSketchLabels.SketchLabels;
OldImageLabels = OldImageLabels.ImageLabels;
SketchData.imdb.images.labels = OldSketchLabels;
ImageData.imdb.images.labels = OldImageLabels;

SketchSet = load('/lustre/koustav.ghosal/ZeroShot/imdb/SketchSet.mat');
ImageSet = load('/lustre/koustav.ghosal/ZeroShot/imdb/ImageSet.mat');
SketchData.imdb.images.set = SketchSet.SketchSet;
ImageData.imdb.images.set = ImageSet.ImageSet;
imagelayer = str2num(imagelayer);
sketchlayer = str2num(sketchlayer);

% Calculate the average feature - both 4096 dimensional for images and
% 512 dimensional for sketches
FullImageFeat.features_caltech = convfeature(FullImageFeat.features,'image');
FullSketchFeat.features = convfeature(FullSketchFeat.features,'sketch');
toc;

% Phase 2: Map the data and form the correspondences between classes
tic;

%Initialize data
LabelMap = mapping.data;
ImageFeaturesTrain = [];
ImageFeaturesTest = [];
SketchFeaturesTest = [];
SketchFeaturesTrain = [];
NewImageLabelsTrain = [];
NewImageLabelsTest = [];
NewSketchLabelsTest = [];
NewSketchLabelsTrain = [];

%ZS part
%Sketches
load('/lustre/koustav.ghosal/ZeroShot/imdb/ZS_TUB.mat');
j=1;
ZS_set=zeros(1,20000);

%Loop over every class
for i=1:250
    disp(i);
    
    % If the class was trained with, exclude it from the retrieval
    if (ZS(setno,i)==1)
        for k=j:j+79
            ZS_set(1,k)=0;
        end
        j=j+80;
        
    % If the class was never seen, i.e. a zero-shot class, then retain the
    % train-test split as designed by the Hospedeles group.
    elseif ZS(setno,i)==0
        for k=j:j+79
            ZS_set(1,k)=SketchData.imdb.images.set(1,k);
        end
        j=j+80;
    end
    
end

% old_set -> original set representation
% sets -> new set - trained classes excluded - zeroshot representation
SketchData.imdb.images.old_set = SketchData.imdb.images.set;
SketchData.imdb.images.set = ZS_set;

%Images
load('/lustre/koustav.ghosal/ZeroShot/imdb/ZS_Caltech.mat');
j=1;
ZS_set=zeros(1,20560);

%Loop over every class
for i=1:257
    disp(i);
    
    % If the class was trained with, exclude it from the retrieval
    if (ZS(setno,i)==1)
        for k=j:j+79
            ZS_set(1,k)=0;
        end
        j=j+80;
        
    % If the class was never seen, i.e. a zero-shot class, then retain the
    % train-test split as designed by us.
    elseif ZS(setno,i)==0
        for k=j:j+79
            ZS_set(1,k)=ImageData.imdb.images.set(1,k);
        end
        j=j+80;
    end
end

% old_set -> original set representation
% sets -> new set - trained classes excluded - zeroshot representation
ImageData.imdb.images.old_set = ImageData.imdb.images.set;
ImageData.imdb.images.set = ZS_set;
%End of ZS part

% Loop for all the 105 corresponding classes
for i = 1:105
    disp(i);
    % For debugging purposes - TBD [Make into an assert statement for final release]
    %currentImageLabel = LabelMap(i,1);
    %currentSketchLabel = LabelMap(i,2);
    
    % Find the indexes of training and testing data in both images and sketches
    % Training -> set == 1 and Testing -> set == 3
    % Also, we want labels of only the selected class in question
    
    idx_images_test = find(ImageData.imdb.images.labels == LabelMap(i,1)...
        & ImageData.imdb.images.set == 3 );
    idx_images_train = find(ImageData.imdb.images.labels == LabelMap(i,1) ...
        & ImageData.imdb.images.set == 1 );
    
    idx_sketches_train = find(SketchData.imdb.images.labels == ...
        LabelMap(i,2) & SketchData.imdb.images.set == 1);
    idx_sketches_test = find(SketchData.imdb.images.labels == ...
        LabelMap(i,2) & SketchData.imdb.images.set == 3);
    
    ImageFeaturesTrain = [ImageFeaturesTrain ;...
        squeeze(FullImageFeat.features_caltech(imagelayer,idx_images_train,:))];
    ImageFeaturesTest = [ImageFeaturesTest ;...
        squeeze(FullImageFeat.features_caltech(imagelayer,idx_images_test,:))];
    
    SketchFeaturesTest = [SketchFeaturesTest ;...
        squeeze(FullSketchFeat.features(sketchlayer,idx_sketches_test,:))];
    SketchFeaturesTrain = [SketchFeaturesTrain ;...
        squeeze(FullSketchFeat.features(sketchlayer,idx_sketches_train,:))];
    
    % Create new labels for the CCA module
    NewImageLabelsTrain = [NewImageLabelsTrain ; i*ones(size(idx_images_train,2),1)];
    NewImageLabelsTest = [NewImageLabelsTest ; i*ones(size(idx_images_test,2),1)];
    NewSketchLabelsTest = [NewSketchLabelsTest ; i*ones(size(idx_sketches_test,2),1)];
    NewSketchLabelsTrain = [NewSketchLabelsTrain ; i*ones(size(idx_sketches_train,2),1)];
end
toc;

% Phase 3 - Training Cluster-CCA
tic;

% Experiment #1 PCA - *Doesn't work*. Reason- Doesn't preserve the learned mapping.
% We would require to use some clustered joint-dimension reduction
% technique

%SketchFeaturesTrain = ReduceDim(SketchFeaturesTrain,100,1);
%SketchFeaturesTest = ReduceDim(SketchFeaturesTest,100,1);
%ImageFeatures = ReduceDim(ImageFeatures,100,1);
toc;

tic;

% Experiment #2 Clustered CCA - Give the features to the cluster-CCA module
[Wx,Wy]=clusterCCA_1(SketchFeaturesTrain,ImageFeaturesTrain,...
    NewSketchLabelsTrain,NewImageLabelsTrain);
toc;

% Phase 4 - Testing the cluster-CCA
tic;

% Project the learned features using the Wx and Wy matrices into the
% subspace
SketchTestFeaturesProjected = (Wx' * SketchFeaturesTest')';
ImageSampleFeaturesProjected = (Wy' * ImageFeaturesTest')';

% Run the retrieval module to get the retrieval results
[Precision, Recall, confusionMatrix] = retrieval(ImageSampleFeaturesProjected,...
    NewImageLabelsTest,SketchTestFeaturesProjected,NewSketchLabelsTest);

save(['/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/' 'PrecisionRecall_' tag '.mat'],'Precision','Recall');
save(['/home/cvit/koustav/Ameya/Zero_shot_Sketch_based_Image_Retrieval/PR_curves/' 'ConfusionMatrix_' tag '.mat'],'confusionMatrix');
toc;
