function [Precision, Recall, confusionMatrix] = retrieval(ImageProjectedFeatures,ImageProjectedLabels,SketchProjectedFeatures,SketchProjectedLabels)
%StructurePath = '/lustre/koustav/BMVC_15/structures/Fisher/';



DatasetSize = size(ImageProjectedFeatures,1);
[ImageSampleCount,ImageUniqueLabel] = hist(ImageProjectedLabels,unique(ImageProjectedLabels));
%[IDX,D] = knnsearch(ImageProjectedFeatures,SketchProjectedFeatures,'k',DatasetSize,'distance','correlation');
[IDX,D] = knnsearch(ImageProjectedFeatures,SketchProjectedFeatures,'k',DatasetSize,'Distance','euclidean');

%disp(size(IDX));
%disp(size(D));
confusionMatrix = zeros(105,105);
PrecisionArray=[];RecallArray=[];
for i=1:size(IDX,1)
    disp(i);
    %thisSketchLabel = SketchLabels(i);
    %thisSketchLabel = find(SketchClass.Index == SketchLabels(i));
    thisSketchLabel = SketchProjectedLabels(i);
    %disp(thisSketchLabel);
    %nSamples = sum(SketchLabels(:)==SketchLabels(i));
    
    cc =0;
    pr = [];re=[];
    for j =1:size(IDX,2)
        if (ImageProjectedLabels(IDX(i,j)) == thisSketchLabel)
            cc=cc+1;
        end

        pr(j) = cc/j;re(j) = cc/ImageSampleCount(find(thisSketchLabel == ImageUniqueLabel));
            
    end
    predictedLabel = mode(ImageProjectedLabels((IDX(i,2:9))));
    confusionMatrix(thisSketchLabel, predictedLabel) = confusionMatrix(thisSketchLabel, predictedLabel) +1 ;

    PrecisionArray(i,:) = pr;RecallArray(i,:) = re;
end

Precision = mean(PrecisionArray,1);Recall = mean(RecallArray,1);
end
