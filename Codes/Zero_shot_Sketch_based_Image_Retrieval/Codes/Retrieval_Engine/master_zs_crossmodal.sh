#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround5_epoch101.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round5_zs_epoch270.mat 1 1 5 ZS_Cross_WVEC_WVEC_set5_1_1
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround5_epoch101.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round5_zs_epoch270.mat 2 2 5 ZS_Cross_WVEC_WVEC_set5_2_2
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround5_epoch101.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round5_zs_epoch270.mat 3 3 5 ZS_Cross_WVEC_WVEC_set5_3_3
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround5_epoch101.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round5_zs_epoch270.mat 4 4 5 ZS_Cross_WVEC_WVEC_set5_4_4

#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround4_epoch95.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round4_zs_epoch223.mat 1 1 4 ZS_Cross_WVEC_WVEC_set4_1_1
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround4_epoch95.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round4_zs_epoch223.mat 2 2 4 ZS_Cross_WVEC_WVEC_set4_2_2
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround4_epoch95.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round4_zs_epoch223.mat 3 3 4 ZS_Cross_WVEC_WVEC_set4_3_3
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround4_epoch95.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round4_zs_epoch223.mat 4 4 4 ZS_Cross_WVEC_WVEC_set4_4_4

sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_round3_zs_epoch94.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round3_zs_epoch285.mat 1 1 3 ZS_Cross_WVEC_WVEC_set3_1_1
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_round3_zs_epoch94.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round3_zs_epoch285.mat 2 2 3 ZS_Cross_WVEC_WVEC_set3_2_2
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_round3_zs_epoch94.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round3_zs_epoch285.mat 3 3 3 ZS_Cross_WVEC_WVEC_set3_3_3
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_round3_zs_epoch94.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round3_zs_epoch285.mat 4 4 3 ZS_Cross_WVEC_WVEC_set3_4_4

#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround2_epoch65.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round2_zs_epoch250.mat 1 1 2 ZS_Cross_WVEC_WVEC_set2_1_1
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround2_epoch65.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round2_zs_epoch250.mat 2 2 2 ZS_Cross_WVEC_WVEC_set2_2_2
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround2_epoch65.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round2_zs_epoch250.mat 3 3 2 ZS_Cross_WVEC_WVEC_set2_3_3
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround2_epoch65.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_wordvec_round2_zs_epoch250.mat 4 4 2 ZS_Cross_WVEC_WVEC_set2_4_4


#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround5_epoch101.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round5_zs_epoch351.mat 1 1 5 ZS_Cross_WVEC_SFTMX_set5_1_1
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround5_epoch101.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round5_zs_epoch351.mat 2 2 5 ZS_Cross_WVEC_SFTMX_set5_2_2
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround5_epoch101.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round5_zs_epoch351.mat 3 3 5 ZS_Cross_WVEC_SFTMX_set5_3_3
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround5_epoch101.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round5_zs_epoch351.mat 4 4 5 ZS_Cross_WVEC_SFTMX_set5_4_4

#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround4_epoch95.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round4_zs_epoch300.mat 1 1 4 ZS_Cross_WVEC_SFTMX_set4_1_1
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround4_epoch95.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round4_zs_epoch300.mat 2 2 4 ZS_Cross_WVEC_SFTMX_set4_2_2
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround4_epoch95.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round4_zs_epoch300.mat 3 3 4 ZS_Cross_WVEC_SFTMX_set4_3_3
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround4_epoch95.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round4_zs_epoch300.mat 4 4 4 ZS_Cross_WVEC_SFTMX_set4_4_4

sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_round3_zs_epoch94.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round3_zs_epoch330.mat 1 1 3 ZS_Cross_WVEC_SFTMX_set3_1_1
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_round3_zs_epoch94.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round3_zs_epoch330.mat 2 2 3 ZS_Cross_WVEC_SFTMX_set3_2_2
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_round3_zs_epoch94.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round3_zs_epoch330.mat 3 3 3 ZS_Cross_WVEC_SFTMX_set3_3_3
sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wordvec_round3_zs_epoch94.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round3_zs_epoch330.mat 4 4 3 ZS_Cross_WVEC_SFTMX_set3_4_4

#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround2_epoch65.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round2_zs_epoch330.mat 1 1 2 ZS_Cross_WVEC_SFTMX_set2_1_1
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround2_epoch65.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round2_zs_epoch330.mat 2 2 2 ZS_Cross_WVEC_SFTMX_set2_2_2
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround2_epoch65.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round2_zs_epoch330.mat 3 3 2 ZS_Cross_WVEC_SFTMX_set2_3_3
#sbatch scriptForRetrieval.sh /lustre/koustav.ghosal/ZeroShot/Features/Caltechfeatures_wvec_zsround2_epoch65.mat /lustre/koustav.ghosal/ZeroShot/Features/TUBfeatures_softmax_round2_zs_epoch330.mat 4 4 2 ZS_Cross_WVEC_SFTMX_set2_4_4