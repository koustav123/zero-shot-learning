#!/bin/bash
#SBATCH -A cvit
#SBATCH -n 4
#SBATCH -p long
#SBATCH --mem=25000
#SBATCH -t 24:00:00

echo $1 $2 $3 $4 $5 $6
/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "calculatePrecisionRecallCrossModal_zs('$1','$2','$3','$4','$5','$6');"

